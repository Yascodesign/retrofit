package net.simplifiedcoding.retrofitandroidtutorial.storage;

import android.content.Context;
import android.content.SharedPreferences;

import net.simplifiedcoding.retrofitandroidtutorial.User;

public class SharedPreferManage {

    private static final String SHARED_PREF_NAME = "my_sahre_preff";

    private static SharedPreferManage mInstance;
    private Context mCtx;

    public SharedPreferManage(Context mCtx) {
        this.mCtx = mCtx;
    }

    public static synchronized SharedPreferManage getmInstance(Context mCtx){

        if(mInstance == null){

            mInstance = new SharedPreferManage(mCtx);
        }
        return mInstance;
    }

    //method to storge the user

    public void saveUser(User user){

        //THIS METHOD TAKE two
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE );
        SharedPreferences.Editor editor =  sharedPreferences.edit();

        editor.putInt("id", user.getId());
        editor.putString("firstname" , user.getFirstname());
        editor.putString("lastname" , user.getLastname());
        editor.putString("email", user.getEmail());
        editor.putString("school", user.getSchool());

        editor.apply();

    }

    public boolean IsLoggedIn(){

        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt("id", -1) != -1;
    }

    public User getUser(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return  new User(
                sharedPreferences.getInt("id", -1),
                sharedPreferences.getString("firstname", null),
                sharedPreferences.getString("lastname", null),
                sharedPreferences.getString("email", null),
                sharedPreferences.getString("school", null)

                );
    }

    public void Clear(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor =  sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}
