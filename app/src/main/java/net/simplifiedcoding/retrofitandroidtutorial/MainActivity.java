package net.simplifiedcoding.retrofitandroidtutorial;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import net.simplifiedcoding.retrofitandroidtutorial.Api.RetrofitClinet;
import net.simplifiedcoding.retrofitandroidtutorial.storage.SharedPreferManage;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;


/*
 * Sign Up Activity
 * */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextEmail, editTextPassword, editTextFName, editTextLName, editTextSchool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextFName = findViewById(R.id.FirstName);
        editTextLName = findViewById(R.id.LasttName);
        editTextSchool = findViewById(R.id.editTextSchool);


        findViewById(R.id.buttonSignUp).setOnClickListener(this);
        findViewById(R.id.textViewLogin).setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(SharedPreferManage.getmInstance(this).IsLoggedIn()){
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
            startActivity(intent);
        }

    }

    private void userSignUp(){
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        String fname = editTextFName.getText().toString().trim();
        String lname = editTextLName.getText().toString().trim();
        String school = editTextSchool.getText().toString().trim();

        if(email.isEmpty()){
            editTextEmail.setError("Email is required");
            editTextEmail.requestFocus();
            return;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            editTextEmail.setError("Enter a valid email");
            editTextEmail.requestFocus();
            return;
        }

        if(password.isEmpty()){
            editTextPassword.setError("Password required");
            editTextPassword.requestFocus();
            return;
        }

        if(password.length() < 6){
            editTextPassword.setError("Password should be atleast 6 character long");
            editTextPassword.requestFocus();
            return;
        }

        if(fname.isEmpty()){
            editTextFName.setError("Name required");
            editTextFName.requestFocus();
            return;
        }

        if(lname.isEmpty()){
            editTextLName.setError("Name required");
            editTextLName.requestFocus();
            return;
        }

        if(school.isEmpty()){
            editTextSchool.setError("School required");
            editTextSchool.requestFocus();
            return;
        }


        /* Do user registration using the api call*/
     Call<ResponseBody> call = RetrofitClinet
             .getmInstance()
             .getApi()
             .createUser(fname,lname,email,school,password);

     call.enqueue(new Callback<ResponseBody>() {
         @Override
         public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

             try {
                 String s = response.body().string();
                 Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT ).show();
             } catch (IOException e) {
                 e.printStackTrace();
             }



         }

         @Override
         public void onFailure(Call<ResponseBody> call, Throwable t) {

             Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();


         }
     });

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.buttonSignUp:
                userSignUp();
                break;
            case R.id.textViewLogin:
                  startActivity(new Intent(this, LoginActivity.class));
                break;
        }
    }


}
