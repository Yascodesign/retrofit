package net.simplifiedcoding.retrofitandroidtutorial.Api;

import net.simplifiedcoding.retrofitandroidtutorial.DefaultResponse;
import net.simplifiedcoding.retrofitandroidtutorial.LoginResponse;
import net.simplifiedcoding.retrofitandroidtutorial.storage.SharedPreferManage;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

    @POST("register.php")
    @FormUrlEncoded
    Call<ResponseBody> createUser(

     @Field("firstname") String firstname,
     @Field("lastname") String lastname,

     @Field("email") String email,
     @Field("school") String school,
     @Field("password") String password

    );

    @FormUrlEncoded
    @POST("login.php")
    Call<LoginResponse> userLogin(
            @Field("email") String email,
            @Field("password") String password
    );

    @GET("users.php")
    Call<UsersResponse> getUsers();

    @FormUrlEncoded
    @POST("update.php/{id}")
    Call<LoginResponse> updateUser(
            @Query("id") int id,
            @Field("firstname") String firstname,
            @Field("lastname") String lastname,
            @Field("email") String email,
            @Field("school") String school

    );
 @DELETE("delete.php")
    Call<DefaultResponse> deleteUser(@Path("id") int id);

}
