package net.simplifiedcoding.retrofitandroidtutorial.Api;

import net.simplifiedcoding.retrofitandroidtutorial.User;

import java.util.List;

public class UsersResponse {


    private List<User>  users;
    private boolean error;

    public UsersResponse(List<User> users, boolean error) {
        this.users = users;
        this.error = error;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
