package net.simplifiedcoding.retrofitandroidtutorial.Api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClinet {

    private  static final String BASE_URL = "http://192.168.43.183/php-restapi/";

     private static  RetrofitClinet mInstance;
     private Retrofit retrofit;

     private  RetrofitClinet(){

         retrofit= new Retrofit.Builder()
                 .baseUrl(BASE_URL)
                 .addConverterFactory(GsonConverterFactory.create())
                 .build();
     }

     //buding synchronized class

    public static synchronized RetrofitClinet getmInstance(){

         if(mInstance==null){
             mInstance =new RetrofitClinet();
         }
         return mInstance;
    }
    public Api getApi(){
         return retrofit.create(Api.class);
    }
}
