package net.simplifiedcoding.retrofitandroidtutorial.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.simplifiedcoding.retrofitandroidtutorial.R;
import net.simplifiedcoding.retrofitandroidtutorial.User;

import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UserViewHolder> {

    private Context mCtx;
    private List<User> userList;

    public UsersAdapter(Context mCtx, List<User> userList) {
        this.mCtx = mCtx;
        this.userList = userList;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(mCtx).inflate(R.layout.recyclerv_users, parent, false);

       return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        User user = userList.get(position);
        holder.textViewFname.setText(user.getFirstname());
        holder.textViewLname.setText(user.getLastname());
        holder.textViewemail.setText(user.getEmail());
        holder.textViewschool.setText(user.getFirstname());

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }


    class UserViewHolder extends RecyclerView.ViewHolder {
        TextView textViewFname, textViewLname, textViewschool,textViewemail;
        public UserViewHolder(View itemView) {
            super(itemView);

          textViewFname = itemView.findViewById(R.id.textViewfName);
          textViewLname = itemView.findViewById(R.id.textViewlName);
            textViewemail = itemView.findViewById(R.id.textViewemail);
            textViewschool = itemView.findViewById(R.id.textViewSchool);


        }
    }

}
