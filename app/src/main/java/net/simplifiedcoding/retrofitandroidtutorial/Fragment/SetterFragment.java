package net.simplifiedcoding.retrofitandroidtutorial.Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import net.simplifiedcoding.retrofitandroidtutorial.Api.RetrofitClinet;
import net.simplifiedcoding.retrofitandroidtutorial.DefaultResponse;
import net.simplifiedcoding.retrofitandroidtutorial.LoginActivity;
import net.simplifiedcoding.retrofitandroidtutorial.LoginResponse;
import net.simplifiedcoding.retrofitandroidtutorial.MainActivity;
import net.simplifiedcoding.retrofitandroidtutorial.R;
import net.simplifiedcoding.retrofitandroidtutorial.User;
import net.simplifiedcoding.retrofitandroidtutorial.storage.SharedPreferManage;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class SetterFragment extends Fragment implements View.OnClickListener{

    private EditText editTextEmail, editTextLName,editTextFName, editTextSchool;
    private EditText editTextCurrentPassword, editTextNewPassword;

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.setter_fragment, container, false);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editTextFName =  view.findViewById(R.id.editTextfName);
        editTextLName = view.findViewById(R.id.editTextlName);
        editTextEmail  = view.findViewById(R.id.editTextEmail);
        editTextSchool = view.findViewById(R.id.editTextSchool);
        editTextCurrentPassword = view.findViewById(R.id.editTextCurrentPassword);
        editTextNewPassword = view.findViewById(R.id.editTextNewPassword);

        view.findViewById(R.id.buttonSave).setOnClickListener(this);
        view.findViewById(R.id.buttonChangePassword).setOnClickListener(this);
        view.findViewById(R.id.buttonDelete).setOnClickListener(this);
        view.findViewById(R.id.buttonLogout).setOnClickListener(this);
    }

    private void updateProfile(){

      String fname = editTextFName.getText().toString().trim();
         String lname = editTextLName.getText().toString().trim();
        String school = editTextSchool.getText().toString().trim();
        String email = editTextEmail.getText().toString().trim();
          if(email.isEmpty()){
            editTextEmail.setError("Email is required");
            editTextEmail.requestFocus();
            return;
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            editTextEmail.setError("Enter a valid email");
            editTextEmail.requestFocus();
            return;
        }


        if(fname.isEmpty()){
            editTextFName.setError("Name required");
            editTextFName.requestFocus();
            return;
        }

        if(lname.isEmpty()){
            editTextLName.setError("Name required");
            editTextLName.requestFocus();
            return;
        }

        if(school.isEmpty()){
            editTextSchool.setError("School required");
            editTextSchool.requestFocus();
            return;
        }

        User user = SharedPreferManage.getmInstance(getActivity()).getUser();
        Call<LoginResponse> call = RetrofitClinet.getmInstance().getApi()
                .updateUser(
                        user.getId(),
                        fname,
                        lname,
                        email,
                        school
                );

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                Toast.makeText(getActivity(), response.body().getMessage(),Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Error: is update " + response.body().getMessage());

                if (!response.body().isError()){
                    SharedPreferManage.getmInstance(getActivity()).saveUser(response.body().getUser() );
                }

                else {
                    Toast.makeText(getActivity(), response.body().getMessage(),Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "Error: is update esle " + response.body().getMessage());

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


    }


    private void logout() {
        SharedPreferManage.getmInstance(getActivity()).Clear();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private  void deleteUser(){

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Are you sure ?");
        builder.setMessage("This action is irreversible ....");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                User user = SharedPreferManage.getmInstance(getActivity()).getUser();
                Call<DefaultResponse>  call = RetrofitClinet.getmInstance().getApi().deleteUser(user.getId());
                call.enqueue(new Callback<DefaultResponse>() {
                    @Override
                    public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {

                        if(!response.body().isErr()){
                            SharedPreferManage.getmInstance(getActivity()).Clear();
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        }
                        Toast.makeText(getActivity(), response.body().getMsg(),Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResponse> call, Throwable t) {

                    }
                });
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog add  = builder.create();
        add.show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonSave:
                  updateProfile();
                break;
            case R.id.buttonChangePassword:

                break;

            case R.id.buttonLogout:
                logout();
                break;

            case R.id.buttonDelete:
                 deleteUser();
                break;

        }
    }
}
