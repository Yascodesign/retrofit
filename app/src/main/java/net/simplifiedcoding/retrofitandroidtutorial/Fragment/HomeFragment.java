package net.simplifiedcoding.retrofitandroidtutorial.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.simplifiedcoding.retrofitandroidtutorial.R;
import net.simplifiedcoding.retrofitandroidtutorial.storage.SharedPreferManage;

public class HomeFragment extends Fragment {

    private TextView textViewEmail, textViewFName,textViewLName, textViewSchool;
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    textViewFName= view.findViewById(R.id.textViewfame);
    textViewLName =view.findViewById(R.id.textViewlName);
    textViewEmail = view.findViewById(R.id.textViewemail);
    textViewSchool =  view.findViewById(R.id.textViewSchool);


    textViewFName.setText(SharedPreferManage.getmInstance(getActivity()).getUser().getFirstname());
    textViewLName.setText(SharedPreferManage.getmInstance(getActivity()).getUser().getLastname());
    textViewEmail.setText(SharedPreferManage.getmInstance(getActivity()).getUser().getEmail());
    textViewSchool.setText(SharedPreferManage.getmInstance(getActivity()).getUser().getSchool());

    }
}
