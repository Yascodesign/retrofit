package net.simplifiedcoding.retrofitandroidtutorial;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import net.simplifiedcoding.retrofitandroidtutorial.Fragment.HomeFragment;
import net.simplifiedcoding.retrofitandroidtutorial.Fragment.SetterFragment;
import net.simplifiedcoding.retrofitandroidtutorial.Fragment.UsersFragment;
import net.simplifiedcoding.retrofitandroidtutorial.storage.SharedPreferManage;

public class ProfileActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

       User user = SharedPreferManage.getmInstance(this).getUser();

        BottomNavigationView navigationView =  findViewById(R.id.button_nav);
        navigationView.setOnNavigationItemSelectedListener(this);

        displayFragment(new HomeFragment());

    }

    private void displayFragment(Fragment fragment){

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.reletivelayout, fragment )
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!SharedPreferManage.getmInstance(this).IsLoggedIn()){
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
            startActivity(intent);
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment =null;
       switch (item.getItemId()){
           case R.id.menu_home:
               fragment = new HomeFragment();
               break;
           case R.id.menu_use:
               fragment = new UsersFragment();
               break;
           case R.id.menu_setting:
               fragment = new SetterFragment();
               break;
       }
      if(fragment !=null){
           displayFragment(fragment);
      }
        return false;
    }
}
